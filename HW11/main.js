const passwordForm = document.querySelector('.password-form');
const errorMessage = document.querySelector('.error-message');
const inputParent = passwordForm.querySelectorAll('.input-wrapper');
const inputItem = document.querySelectorAll('.input-password');
let submitBtn = passwordForm.querySelector('.btn-submit');

inputParent.forEach(function () {
  return addEventListener('click', changePasswordVisibility);
});

submitBtn = addEventListener('click', comparePassword);

function changePasswordVisibility(icon) {
  const eyeIcon = icon.target;
  if (eyeIcon.nodeName === 'I' && eyeIcon.classList.contains('icon-password')) {
    const inputPassword = eyeIcon.parentElement.querySelector('.input-password');
    eyeIcon.classList.toggle('fa-eye-slash');
    eyeIcon.classList.toggle('fa-eye');
    inputPassword.type = (inputPassword.type === 'password') ? 'text' : 'password';
  }
}

function comparePassword(userPassword) {
  const submit = userPassword.target;
  if (submit.nodeName === 'BUTTON' && submit.classList.contains('btn-submit')) {
    userPassword.preventDefault();
    if (inputItem[0].value === '') {
      errorMessage.innerText = 'Enter your password';
      errorMessage.style.color = '#170A1C';
    }
    else if (inputItem[0].value === inputItem[1].value) {
      errorMessage.innerText = '';
      alert(`You are welcome`);
    } else {
      errorMessage.innerText = 'Passwords do not match';
      errorMessage.style.color = '#BF3100';
    }
  }

}
