const button = document.querySelector("button");
const body = document.body;
const themeKey = "theme";
const brightTheme = "bright-background";
const darkTheme = "dark-background";

// load the saved theme on page load
loadTheme();

// add click event listener to the button
button.addEventListener("click", toggleTheme);

function toggleTheme() {
    if (body.classList.contains(darkTheme)) {

        // switch to bright theme
        body.classList.toggle(darkTheme);
        body.classList.toggle(brightTheme);
        button.innerHTML = "Dark Theme";
        button.style.color = "#979797"; //Grey
        button.style.backgroundColor = "#121111"; //Black

        // save the theme preference
        sessionStorage.setItem(themeKey, brightTheme);
    } else {

        // switch to dark theme
        body.classList.toggle(brightTheme);
        body.classList.toggle(darkTheme);
        button.innerHTML = "Bright Theme";
        button.style.color = "#241313"; //Black
        button.style.backgroundColor = "#979797"; //Grey

        // save the theme preference
        sessionStorage.setItem(themeKey, darkTheme);
    }
}

function loadTheme() {
    const savedTheme = sessionStorage.getItem(themeKey);
    if (savedTheme === brightTheme) {
        body.classList.add(brightTheme);
        button.innerHTML = "Dark Theme";
        button.style.color = "#979797"; //Grey
        button.style.backgroundColor = "#121111"; //Black
    } else if (savedTheme === darkTheme) {
        body.classList.add(darkTheme);
        button.innerHTML = "Bright Theme";
        button.style.color = "#241313"; //Black
        button.style.backgroundColor = "#979797"; //Grey
    }
}

console.log(sessionStorage.getItem(themeKey));