1. Які існують типи даних у Javascript?

Undefined;
Boolean;
Number;
String;
BigInt;
Symbol;
Null;
Object;

2. У чому різниця між == і ===?

Символ == порівнює значення по можливості конвертуючи їх до одного типу, 
а === порівнює значення та іх тип без конвертації.

3. Що таке оператор?

Оператор це внутрішня функція JS. Прикладом операторів може бути додавання, 
множення, або знаходження залишку від числа.
