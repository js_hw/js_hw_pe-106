
function filterBy(arr, varType) {
    let filteredArr = arr.filter((value) => (typeof value) !== (typeof varType));
    return filteredArr;
}
let mainArray = ['hello', 'world', 23, '23', null];
let userType = 'string';

console.log(filterBy(mainArray, userType));