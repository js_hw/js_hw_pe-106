const tabs = document.querySelectorAll('.tabs-title');
const contents = document.querySelectorAll('.tab-element');

for (let a = 0; a < tabs.length; a++) {
    tabs[a].addEventListener("click", (event) => {
        // remove active class only from tabs
        let tabsChildren = event.target.parentElement.children;
        for (let b = 0; b < tabsChildren.length; b++) {
            tabsChildren[b].classList.remove("active");
        }

        // adding active class 
        tabs[a].classList.add("active");

        // removing active class from tab content blocks
        let tabContentChildren = event.target.parentElement.nextElementSibling.children;
        for (let c = 0; c < tabContentChildren.length; c++) {
            tabContentChildren[c].classList.remove("active");
        }

        // adding active class 
        contents[a].classList.add("active");

    });
}


// let ul = document.querySelector('.tabs');
// ul.addEventListener('click', function (ev) {
//     console.log(ev.target.dataset.type); let data = ev.target.dataset.type;
//     console.log(document.querySelector('.active-p'));
//     document.querySelector('.active-p').classList.remove('active-p');
//     console.log(document.querySelector('.active-tab'));
//     document.querySelector('.active-tab').classList.remove('active-tab');
//     console.log(document.querySelector(`[data-li = ${data}]`));
//     document.querySelector(`[data-li = ${data}]`).classList.add('active-p'); ev.target.classList.add('active-tab')
// });