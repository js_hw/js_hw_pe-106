let userNumber = prompt("Please enter your number", '');
while (userNumber === null || userNumber === "" || isNaN(userNumber)) {
  userNumber = prompt("You need to enter number to continue", '');
}

if (userNumber < 5) {
  alert("Sorry, no numbers");
}

for (let i = 0; i <= userNumber; i++) {
  if (i % 5 === 0) {
    console.log(i);
  }
}


