const carouselItemsPhoto = document.querySelector('.carousel__items-photo');
const itemPhotoAll = document.querySelectorAll('.carousel__items-photo .item-photo');
const sliderItems = document.querySelector('.about-slider__items');
const prevBtn = document.querySelector('.carousel__prev');
const nextBtn = document.querySelector('.carousel__next');

const carouselItemsPhotoArray = [];
let indexCarouselPhotoActive;
const indexesCarouselPhotos = [];
const countAddPhotoCarousel = 4;
const valuePxMove = 80;
const valuePxDefault = -160;

function clearStateActive(items) {
  [...items].forEach((item) => {
    item.classList.remove(nameCssActive);
  });
}

function setStateActive(item) {
  item.classList.add(nameCssActive);
}

function getStateActive(items) {
  return [...items].find((item) => item.classList.contains(nameCssActive));
}

function initSlider() {
  for (let i = 0; i < itemPhotoAll.length; i++) {
    carouselItemsPhotoArray.push(itemPhotoAll[i]);
    if (itemPhotoAll[i].classList.contains(nameCssActive)) {
      indexCarouselPhotoActive = i;
    }
    itemPhotoAll[i].remove();
  }
  setIndexesCarouselPhotos();
}

function setIndexesCarouselPhotos() {
  indexesCarouselPhotos.length = 0;
  carouselItemsPhoto.innerHTML = '';
  for (
    let i = indexCarouselPhotoActive - countAddPhotoCarousel;
    i <= indexCarouselPhotoActive + countAddPhotoCarousel;
    i++
  ) {
    let item;
    if (i < 0) {
      item = carouselItemsPhotoArray.length + i;
    }
    if (i >= 0 && i < carouselItemsPhotoArray.length) {
      item = i;
    }
    if (i >= carouselItemsPhotoArray.length) {
      item = i - carouselItemsPhotoArray.length;
    }
    indexesCarouselPhotos.push(item);
    carouselItemsPhoto.append(carouselItemsPhotoArray[item]);
  }
}

function handlerCarouselPhoto(e) {
  const element = e.target.closest('.item-photo');
  if (element) {
    if (element.classList.contains(nameCssActive)) {
      return;
    }

    const step =
      indexesCarouselPhotos.indexOf(Number(element.dataset.item)) -
      countAddPhotoCarousel;

    handlerMoveCarousel(-step, step);
  } else {
    alert('Error: not element in handlerCarouselPhoto(e)');
  }
}

function handlerMoveCarousel(count, step) {
  carouselItemsPhoto.style.transition = 'all ease 0.3s';
  carouselItemsPhoto.style.transform = `translateX(${valuePxDefault + valuePxMove * count
    }px)`;
  indexCarouselPhotoActive = indexesCarouselPhotos[countAddPhotoCarousel + step];
  clearStateActive(carouselItemsPhotoArray);
  setStateActive(carouselItemsPhotoArray[indexCarouselPhotoActive]);

  setTimeout(() => {
    carouselItemsPhoto.removeAttribute('style');
    setIndexesCarouselPhotos();
    setActiveContent(carouselItemsPhotoArray[indexCarouselPhotoActive]);
  }, 350);
}

function setActiveContent(item) {
  const contentItem = getInfoConnectedContent(item);

  if (contentItem) {
    clearStateActive(sliderItems.children);
    setStateActive(contentItem);
  } else {
    alert('Error: not finded connectedContentItem');
  }
}

function getInfoConnectedContent(item) {
  for (let i of sliderItems.children) {
    if (i.dataset.item === item.dataset.item) {
      return i;
    }
  }
  return false;
}

prevBtn.addEventListener('click', () => handlerMoveCarousel(1, -1));
nextBtn.addEventListener('click', () => handlerMoveCarousel(-1, 1));
carouselItemsPhoto.addEventListener('click', handlerCarouselPhoto);

initSlider();
