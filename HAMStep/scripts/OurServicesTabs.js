const tabs = document.querySelectorAll('.tabs-menu__item-link');
const contents = document.querySelectorAll('.tabs-content__item');

for (let a = 0; a < tabs.length; a++) {
    tabs[a].addEventListener("click", (event) => {
        let tabsChildren = event.target.parentElement.children;
        for (let b = 0; b < tabsChildren.length; b++) {
            tabsChildren[b].classList.remove("active");
        }

        tabs[a].classList.add("active");
        
        let tabContentChildren = event.target.parentElement.nextElementSibling.children;
        for (let c = 0; c < tabContentChildren.length; c++) {
            tabContentChildren[c].classList.remove("active");
        }
        contents[a].classList.add("active");

    });
}
