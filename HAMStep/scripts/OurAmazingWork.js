const imagesMenuItemLink = 'images-menu__item-link';
const countItemsContent = [12, 19, 33];
const maxCountPushLoadBtn = 2;
let currentCountPushLoadBtn = 0;

const imagesMenu = document.querySelector('.our-amazing-work__images-menu');
const imagesContent = document.querySelector('.our-amazing-work__images-content');
const imagesLoadMoreBtn = document.querySelector('.our-amazing-work__button');
const loaderWrapper = document.querySelector('.loader-wrapper');
const nameCssActive = 'active';

const all = {
    textForFirstItem: 'All Projects',
    datasetValue: 'all-projects',
};
const graphicDesign = {
    textForFirstItem: 'Graphic Design',
    datasetValue: 'graphic-design',
    path: './images/graphicDesign/',
    items: [
        'graphic-design1.jpg',
        'graphic-design2.jpg',
        'graphic-design3.jpg',
        'graphic-design4.jpg',
        'graphic-design5.jpg',
        'graphic-design6.jpg',
        'graphic-design7.jpg',
        'graphic-design8.jpg',
        'graphic-design9.jpg',
        'graphic-design10.jpg',
        'graphic-design11.jpg',
        'graphic-design12.jpg',
    ],
};
const webDesign = {
    textForFirstItem: 'Web Design',
    datasetValue: 'web-design',
    path: './images/webDesign/',
    items: [
        'web-design1.jpg',
        'web-design2.jpg',
        'web-design3.jpg',
        'web-design4.jpg',
        'web-design5.jpg',
        'web-design6.jpg',
        'web-design7.jpg',
    ]
};
const landingPages = {
    textForFirstItem: 'Landing Pages',
    datasetValue: 'landing-pages',
    path: './images/landingPage/',
    items: [
        'landing-page1.jpg',
        'landing-page2.jpg',
        'landing-page3.jpg',
        'landing-page4.jpg',
        'landing-page5.jpg',
        'landing-page6.jpg',
        'landing-page7.jpg',
    ],
};
const wordpress = {
    textForFirstItem: 'Wordpress',
    datasetValue: 'wordpress',
    path: './images/wordPress/',
    items: [
        'wordpress1.jpg',
        'wordpress2.jpg',
        'wordpress3.jpg',
        'wordpress4.jpg',
        'wordpress5.jpg',
        'wordpress6.jpg',
        'wordpress7.jpg',
        'wordpress8.jpg',
        'wordpress9.jpg',

    ],
};

function clearStateActive(items) {
    [...items].forEach((item) => {
        item.classList.remove(nameCssActive);
    });
}

function setStateActive(item) {
    item.classList.add(nameCssActive);
}

function getStateActive(items) {
    return [...items].find((item) => item.classList.contains(nameCssActive));
}


function handlerImagesMenu(e) {
    const item = e.target;

    if (
        item.classList.contains(imagesMenuItemLink) &&
        !item.classList.contains(nameCssActive)
    ) {
        currentCountPushLoadBtn = 0;
        imagesLoadMoreBtn.classList.remove('disable');
        clearStateActive(imagesMenu.children);
        setStateActive(item);
        changeImagesContent();
    }
}

function handlerLoadMoreBtn() {
    loaderWrapper.classList.add('enable');
    setTimeout(() => {
        loaderWrapper.style.opacity = 1;
    }, 300);

    setTimeout(() => {
        setTimeout(() => {
            loaderWrapper.classList.remove('enable');
        }, 1000);
        loaderWrapper.style.opacity = 0;

        currentCountPushLoadBtn++;

       
        changeImagesContent();
      

        if (currentCountPushLoadBtn === maxCountPushLoadBtn) {
            imagesLoadMoreBtn.classList.add('disable');
        }
    }, 3000);
}

function changeImagesContent() {
    const imagesMenuItemActive = getStateActive(imagesMenu.children);
    const nameObjItems = getObjNameFromDataset(imagesMenuItemActive);

    if (nameObjItems) {
        let i = 0;
        let countImage = 0;

        clearImagesContent();

        for (
            let count = 0;
            count < countItemsContent[currentCountPushLoadBtn];
            count++
        ) {
            i = Math.floor(Math.random() * nameObjItems.length);

            imagesContent.insertAdjacentHTML(
                'beforeend',
                imageItemContent(
                    nameObjItems[i].textForFirstItem,
                    nameObjItems[i].datasetValue,
                    nameObjItems[i].path,
                    nameObjItems[i].items[countImage]
                )
            );
            countImage =
                countImage >= nameObjItems[i].items.length - 1 ? 0 : countImage + 1;
        }
    } else {
        alert('Error: not found nameObjItems');
    }
}

function clearImagesContent() {
    imagesContent.innerHTML = '';
}

function imageItemContent(text, datasetValue, path, name) {
    return `
  <div class="images-content__item" data-images-item-link="${datasetValue}">
    <div class="images-content__item-front">
      <div class="images-content__item-img">
        <img src="${path}${name}" alt="${text}" />
      </div>
    </div>

    <div class="images-content__item-back">
      <div class="images-content__item-text">
        <ul class="item-text__circles">
        <a href="#">
          <li class="button-circle-white">
            <i class="fa-sharp fa-solid fa-link"></i>
          </li>
          </a>
          <a href="#">
          <li class="button-circle-green">
            <i class="fa-sharp fa-solid fa-square"></i>
          </li>
          </a>
        </ul>
        <div class="item-text__title">
          creative design
        </div>
        <div class="item-text__subtitle">${text}</div>
      </div>
    </div>
  </div>
  `;
}

function getObjNameFromDataset(item) {
    let result;

    switch (item.dataset.imagesMenuLink) {
        case 'all-projects':
            result = [graphicDesign, webDesign, landingPages, wordpress];
            break;
        case 'graphic-design':
            result = [graphicDesign];
            break;
        case 'web-design':
            result = [webDesign];
            break;
        case 'landing-pages':
            result = [landingPages];
            break;
        case 'wordpress':
            result = [wordpress];
            break;
        default:
            result = false;
    }
    return result;
}

imagesMenu.addEventListener('click', handlerImagesMenu);
imagesLoadMoreBtn.addEventListener('click', handlerLoadMoreBtn);


