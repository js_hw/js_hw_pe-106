// 1

const paragraphColorChange = document.querySelectorAll("p");

// console.log(paragraphColorChange);

for (let pList of paragraphColorChange) {
    pList.style.backgroundColor = '#ff0000';
};


// 2 

const idSearch = document.getElementById('optionsList');

console.log(idSearch);

for (let i = 0; i < idSearch.children.length; ++i) {
    console.log(idSearch.children[i]);
  }

// 3 

const paragraphNameChange = document.getElementById('testParagraph');

paragraphNameChange.id = 'This is a paragraph';

console.log(paragraphNameChange);


// 4 && 5

const header = document.querySelector('.main-header');
const headerChildren = header.children;

for (let elem of headerChildren) {
    //.classList to manipulate class only
    elem.classList.add('nav-item');
    console.log(elem);
};


// 6 

const classRemove = document.querySelectorAll('.section-title');

for (let elem of classRemove) {
    //.classList to manipulate class only
    elem.classList.toggle('section-title');
}

