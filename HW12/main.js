const btn = Array.from(document.querySelectorAll(".btn"));

document.addEventListener("keydown", (event) => {
  btn.forEach((element) => {
    if (element.innerHTML.toLowerCase() == event.key.toLowerCase()) {
      element.classList.add("highlight");
    } else {
      element.classList.remove("highlight");
    }
  });
});
