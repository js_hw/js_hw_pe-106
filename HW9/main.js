const yourArray = ["Hello", "World", "Kyiv", "Kharkiv", "Odessa", "Lviv", "Hello", "World", "Kyiv", "Kharkiv", "Odessa", "Lviv"];
const yourParentElement = document.getElementById('list');


//parent = document.body means that second argument is optional, if we won`t choose to which list we want to add elements it will be added to the body element of the page
function createList(array, parent = document.body) {

    array.map(function (element) {
        const li = document.createElement('li');
        li.innerText = element;
        parent.appendChild(li);
    });

}

createList(yourArray, yourParentElement);

